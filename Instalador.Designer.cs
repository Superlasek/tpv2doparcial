﻿namespace TPV2doParcial
{
    partial class Instalador
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Instalador));
            this.label1 = new System.Windows.Forms.Label();
            this.buttonCompleta = new System.Windows.Forms.Button();
            this.buttonPersonalizada = new System.Windows.Forms.Button();
            this.textBoxInfo = new System.Windows.Forms.TextBox();
            this.labelNombreArchivo = new System.Windows.Forms.Label();
            this.buttonLimpiar = new System.Windows.Forms.Button();
            this.buttonGuardar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Visualizador de Instrucciones";
            // 
            // buttonCompleta
            // 
            this.buttonCompleta.FlatAppearance.BorderColor = System.Drawing.Color.Salmon;
            this.buttonCompleta.FlatAppearance.MouseDownBackColor = System.Drawing.Color.IndianRed;
            this.buttonCompleta.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkSalmon;
            this.buttonCompleta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCompleta.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCompleta.Location = new System.Drawing.Point(524, 249);
            this.buttonCompleta.Name = "buttonCompleta";
            this.buttonCompleta.Size = new System.Drawing.Size(140, 57);
            this.buttonCompleta.TabIndex = 2;
            this.buttonCompleta.Text = "Instalación completa";
            this.buttonCompleta.UseVisualStyleBackColor = true;
            this.buttonCompleta.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonPersonalizada
            // 
            this.buttonPersonalizada.FlatAppearance.BorderColor = System.Drawing.Color.Salmon;
            this.buttonPersonalizada.FlatAppearance.MouseDownBackColor = System.Drawing.Color.IndianRed;
            this.buttonPersonalizada.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkSalmon;
            this.buttonPersonalizada.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPersonalizada.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPersonalizada.Location = new System.Drawing.Point(524, 188);
            this.buttonPersonalizada.Name = "buttonPersonalizada";
            this.buttonPersonalizada.Size = new System.Drawing.Size(140, 55);
            this.buttonPersonalizada.TabIndex = 4;
            this.buttonPersonalizada.Text = "Instalación personalizada";
            this.buttonPersonalizada.UseVisualStyleBackColor = true;
            this.buttonPersonalizada.Click += new System.EventHandler(this.buttonPersonalizada_Click);
            // 
            // textBoxInfo
            // 
            this.textBoxInfo.AllowDrop = true;
            this.textBoxInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(135)))));
            this.textBoxInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxInfo.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInfo.ForeColor = System.Drawing.SystemColors.Window;
            this.textBoxInfo.Location = new System.Drawing.Point(22, 90);
            this.textBoxInfo.Multiline = true;
            this.textBoxInfo.Name = "textBoxInfo";
            this.textBoxInfo.ReadOnly = true;
            this.textBoxInfo.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxInfo.Size = new System.Drawing.Size(496, 256);
            this.textBoxInfo.TabIndex = 5;
            this.textBoxInfo.TextChanged += new System.EventHandler(this.textBoxInfo_TextChanged);
            this.textBoxInfo.DragDrop += new System.Windows.Forms.DragEventHandler(this.textBoxInfo_DragDrop);
            this.textBoxInfo.DragEnter += new System.Windows.Forms.DragEventHandler(this.textBoxInfo_DragEnter);
            // 
            // labelNombreArchivo
            // 
            this.labelNombreArchivo.AutoSize = true;
            this.labelNombreArchivo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNombreArchivo.Location = new System.Drawing.Point(19, 69);
            this.labelNombreArchivo.Name = "labelNombreArchivo";
            this.labelNombreArchivo.Size = new System.Drawing.Size(0, 15);
            this.labelNombreArchivo.TabIndex = 6;
            this.labelNombreArchivo.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // buttonLimpiar
            // 
            this.buttonLimpiar.FlatAppearance.BorderColor = System.Drawing.Color.Salmon;
            this.buttonLimpiar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.IndianRed;
            this.buttonLimpiar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkSalmon;
            this.buttonLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLimpiar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLimpiar.Location = new System.Drawing.Point(601, 320);
            this.buttonLimpiar.Name = "buttonLimpiar";
            this.buttonLimpiar.Size = new System.Drawing.Size(63, 26);
            this.buttonLimpiar.TabIndex = 7;
            this.buttonLimpiar.Text = "Limpiar";
            this.buttonLimpiar.UseVisualStyleBackColor = true;
            this.buttonLimpiar.Click += new System.EventHandler(this.buttonLimpiar_Click);
            // 
            // buttonGuardar
            // 
            this.buttonGuardar.FlatAppearance.BorderColor = System.Drawing.Color.SlateBlue;
            this.buttonGuardar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.MediumPurple;
            this.buttonGuardar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkSlateBlue;
            this.buttonGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGuardar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonGuardar.Location = new System.Drawing.Point(524, 320);
            this.buttonGuardar.Name = "buttonGuardar";
            this.buttonGuardar.Size = new System.Drawing.Size(71, 26);
            this.buttonGuardar.TabIndex = 10;
            this.buttonGuardar.Text = "Guardar";
            this.buttonGuardar.UseVisualStyleBackColor = true;
            this.buttonGuardar.Visible = false;
            this.buttonGuardar.Click += new System.EventHandler(this.buttonGuardar_Click);
            // 
            // Instalador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Linen;
            this.ClientSize = new System.Drawing.Size(686, 361);
            this.Controls.Add(this.buttonGuardar);
            this.Controls.Add(this.buttonLimpiar);
            this.Controls.Add(this.labelNombreArchivo);
            this.Controls.Add(this.textBoxInfo);
            this.Controls.Add(this.buttonPersonalizada);
            this.Controls.Add(this.buttonCompleta);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Instalador";
            this.Opacity = 0.95D;
            this.Text = "Instalador  |  Stephano Bravo (Lasek)";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonCompleta;
        private System.Windows.Forms.Button buttonPersonalizada;
        private System.Windows.Forms.TextBox textBoxInfo;
        private System.Windows.Forms.Label labelNombreArchivo;
        private System.Windows.Forms.Button buttonLimpiar;
        private System.Windows.Forms.Button buttonGuardar;
    }
}

