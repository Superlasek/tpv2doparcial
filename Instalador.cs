﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

/*
 * Alexis Carlos Stephano Bravo Castañeda
 * Lasek
 * UNEDL | 4°A T/M
 */

namespace TPV2doParcial
{
    public partial class Instalador : Form
    {
        private string[] entrada;
        private Dictionary<int, string> parsed;//Diccionario para parsear
        private bool ignComm = true;
        //Path al directorio dentro del proyecto
        private string path = Path.Combine(Directory.GetCurrentDirectory(), "segundoexamenparcial.txt");
        public Instalador()
        {
            InitializeComponent();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            buttonPersonalizada.Enabled = false;
            buttonCompleta.Enabled = false;
            buttonGuardar.Visible = true;
            textBoxInfo.ReadOnly = false;
            //string text = System.IO.File.ReadAllText(@"C:\Users\Lasek\UNEDL\UNEDL2020B\4to\Windows\TPV2doParcial\segundoexamenparcial.txt");
            //string path = @"C:\Users\Lasek\UNEDL\UNEDL2020B\4to\Windows\TPV2doParcial\segundoexamenparcial.txt";
            //string[] lines = System.IO.File.ReadAllLines(@"C:\Users\Lasek\UNEDL\UNEDL2020B\4to\Windows\TPV2doParcial\segundoexamenparcial.txt");
            //StreamReader stream = new StreamReader(path);
            //string archivoDatos = stream.ReadToEnd();
            //textBoxInfo.Text = archivoDatos.ToString();
            StreamReader stream = new StreamReader(path);
            string archivoDatos = stream.ReadToEnd();
            textBoxInfo.Multiline = true;
            textBoxInfo.Text = archivoDatos;
            stream.Close();
        }

        private void buttonPersonalizada_Click(object sender, EventArgs e)
        {
            textBoxInfo.ReadOnly = false;
            buttonPersonalizada.Enabled = false;
            buttonCompleta.Enabled = false;
            buttonGuardar.Visible = true;

            StreamReader stream = new StreamReader(path);
            string archivoDatos = stream.ReadToEnd();
            textBoxInfo.Multiline = true;
            entrada = archivoDatos.Split(Environment.NewLine.ToCharArray()).ToArray();
            parsed = parseadoEntrada();
            textBoxInfo.Text = imprimirParseado();
            stream.Close();

        }

        private void textBoxInfo_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        private void textBoxInfo_DragDrop(object sender, DragEventArgs e)
        {
            // Arreglo con los archivos
            string[] archivos = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            string linea = "";

            labelNombreArchivo.Text = archivos[0];

            // Leer archivo
            StreamReader lector = File.OpenText(archivos[0]);

            while((linea=lector.ReadLine()) != null)
            {
                buttonGuardar.Visible = true;
                textBoxInfo.Text += linea + "\r\n";
            }

            lector.Close();
        }

        private void buttonLimpiar_Click(object sender, EventArgs e)
        {
            limpiarxd();

        }

        public void limpiarxd()
        {
            textBoxInfo.Clear();
            labelNombreArchivo.Text = "";
            buttonCompleta.Enabled = true;
            buttonPersonalizada.Enabled = true;

            buttonGuardar.Visible = false;
            textBoxInfo.ReadOnly = true;
        }

        /*private void cbxInstall_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedValue = cbxInstall.GetItemText(cbxInstall.SelectedItem);
            actualizarInformacion(selectedValue, ".option");
        }*/
        private void textBoxInfo_TextChanged(object sender, EventArgs e)
        {
            // Idea
            //String com = "#" + "-";
            //com = com.Replace("","");


            /*IDictionary<string, string> values = new Dictionary<string, string>();
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\Lasek\UNEDL\UNEDL2020B\4to\Windows\TPV2doParcial\segundoexamenparcial.txt");
            foreach (string line in lines)
            {
                if (line.StartsWith("#"))
                {
                    // es un comentario
                    continue;
                }
                textBoxInfo.Text = lines.ToString();
            }*/
        }

        // Diccionario para evitar los comentarios
        private Dictionary<int, string> parseadoEntrada()
        {
            Dictionary<int, string> output = new Dictionary<int, string>();
            for (int i = 0; i < entrada.Length; i++)
            {
                // Ignora si el input es #
                if (entrada[i].Length != 0 && entrada[i][0] != '#')
                {
                    output.Add(i, entrada[i]);
                }
            }
            return output;
        }

        private String imprimirParseado()
        {
            List<string> output = new List<string>();

            var keys = parsed.Keys.ToList();
            keys.Sort();

            foreach (var key in keys)
            {
                output.Add(parsed[key]);
            }

            return string.Join(Environment.NewLine, output);

        }

        private void buttonGuardar_Click(object sender, EventArgs e)
        {
            //Guardar output en archivo nuevo
            //El archivo se encuentra abierto en este punto
            var output = new List<string>();
            string[] lines = textBoxInfo.Text.Split(Environment.NewLine.ToCharArray()).ToArray();
            if (!ignComm)
            {
                var userTextQ = new Queue<string>(lines);
                for (int i = 0; i < entrada.Length; i++)
                {
                    if (parsed.ContainsKey(i))
                    {
                        output.Add(userTextQ.Dequeue());
                    }
                    else
                    {
                        output.Add(entrada[i]);
                    }
                }
            }
            else
            {
                output = lines.ToList();
            }

            //Escribir en el archivo los nuevos cambios
            System.Diagnostics.Debug.WriteLine(string.Join(Environment.NewLine, output));
            //Path a donde se va a guardar
            System.IO.File.WriteAllText(@"C:\Users\Lasek\UNEDL\UNEDL2020B\4to\Windows\TPV2doParcial\ArchivoSaliente.txt", string.Join(Environment.NewLine, output));
            MessageBox.Show("El archivo se ha guardado con el nombre: 'ArchivoSaliente.txt'\nEn la carpeta del proyecto.", "Instalador");
            limpiarxd();
        }

    }
}